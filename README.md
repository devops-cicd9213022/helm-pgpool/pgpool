<<<<<<< README.md
- **Mã hóa secret**: (Cần cài `kubeseal`) Lấy đường dẫn của public key của cluster tương ứng ở thư mục `secrets` và chạy lệnh sau

  - Với mã hóa dạng file

  ```sh
    kubeseal --raw --scope cluster-wide --from-file=[đường dẫn file cần mã hóa] --cert=[đường dân file public key]
  ```
  - Với mã hóa dạng chuỗi

  ```sh
    echo -n "[chuỗi cần mã hóa]" | kubeseal --raw --scope cluster-wide --from-file=/dev/stdin --cert=[đường dân file public key]
  ```

- **Cài đặt kubeseal**:

  ```sh
    wget "https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.24.1/kubeseal-0.24.1-linux-amd64.tar.gz"
    tar -xvzf kubeseal-0.24.1-linux-amd64.tar.gz kubeseal
    sudo install -m 755 kubeseal /usr/local/bin/kubeseal
  ```

- **Download .pem kubeseal**:

  ```sh
    kubeseal --controller-namespace secrets-system --controller-name sealed-secrets --fetch-cert >mycert.pem
  ```

- **Test Helm**:
  ```sh
    helm upgrade --install --namespace=test-helm --create-namespace kibana kibana -f kibana/values.yaml -f kibana/values-dev.yaml --set app.image.tag=dev.ce3de608
    helm template kibana kibana -f kibana/values.yaml -f kibana/values-dev.yaml --set app.image.tag=dev.ce3de608
  ```

kubeseal --controller-namespace secrets-system --controller-name sealed-secrets --fetch-cert >mycert.pem
