{{/*
Sinh YAML cho container

Input là object có cấu trúc giống `app` trong value của common chart
với trường đặc biệt `.top` là toàn bộ value của chart extend

Cách dùng: {{ dict "top" $top | deepCopy | merge $app | template "common.container" }}
*/}}
{{- define "common.container" -}}

name: {{ default "main" .name }}
{{- if .image }}
image: {{ default $.top.Values.global.app.image.repository .image.repository }}:{{ default $.top.Values.global.app.image.tag .image.tag }}
imagePullPolicy: {{ default "IfNotPresent" .image.pullPolicy }}
{{- end }}
{{- with .command }}
command:
  {{- tpl (toYaml .) $.top | nindent 2 }}
{{- end }}
{{- with .args }}
args:
  {{- tpl (toYaml .) $.top | nindent 2 }}
{{- end }}
{{- with .lifecycle }}
lifecycle:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .env }}
env:
  {{- tpl (toYaml .) $.top | nindent 2 }}
{{- end }}
envFrom:
  {{- range .envFrom }}
  {{- if eq .type "configMap" }}
  - configMapRef:
      {{- if .name }}
      name: {{ tpl .name $.top }}
      {{- else if .parentNameSuffix}}
      name: {{ list $.top .parentNameSuffix | include "common.parentElementName" }}
      {{- else }}
      name: {{ list $.top .nameSuffix | include "common.elementName" }}
      {{- end }}
  {{- else if eq .type "secret" }}
  - secretRef:
      {{- if .name }}
      name: {{ tpl .name $.top }}
      {{- else if .parentNameSuffix}}
      name: {{ list $.top .parentNameSuffix | include "common.parentElementName" }}
      {{- else }}
      name: {{ list $.top .nameSuffix | include "common.elementName" }}
      {{- end }}
  {{- end }}
    prefix: {{ .prefix }}
  {{- end }}
volumeMounts:
  {{- range .volumeMounts }}
  {{- if .statefulset }}
  - mountPath: {{ .mountPath }}
    {{- if .name }}
    name: {{ tpl .name $.top }}
    {{- else }}
    name: {{ list $.top .nameSuffix | include "common.elementName" }}
    {{- end }}
  {{- else }}
  - {{ toYaml . | nindent 4 | trim }}
  {{- end }}
  {{- end }}
ports:
  {{- range .ports }}
  - containerPort: {{ .internal }}
    name: {{ .name }}
    protocol: {{ default "TCP" .protocol }}
  {{- end }}
livenessProbe:
  {{- block "common.container.liveness" . }}
  {{- range .ports }}
  {{- $port := . }}
  {{- if .healthCheck }}
  {{- with .healthCheck.liveness }}
  {{- if eq .type "http" }}
  httpGet:
    path: {{ default "/" .endpoint }}
    port: {{ $port.name }}
  {{- else if eq .type "tcp" }}
  tcpSocket:
    port: {{ $port.name }}
  {{- else if eq .type "command" }}
  exec:
    command:
      {{ .command | toYaml | nindent 6 }}
  {{- end }}
  failureThreshold: {{ default 3 .failureThreshold }}
  initialDelaySeconds: {{ default 10 .initialDelaySeconds }}
  periodSeconds: {{ default 30 .periodSeconds }}
  successThreshold: {{ default 1 .successThreshold }}
  timeoutSeconds: {{ default 5 .timeoutSeconds }}
  {{- end }}
  {{- end }}
  {{- end }}
  {{- end }}
readinessProbe:
  {{- block "common.container.readiness" . }}
  {{- range .ports }}
  {{- $port := . }}
  {{- if .healthCheck }}
  {{- with .healthCheck.readiness }}
  {{- if eq .type "http" }}
  httpGet:
    path: {{ default "/" .endpoint }}
    port: {{ $port.name }}
  {{- else if eq .type "tcp" }}
  tcpSocket:
    port: {{ $port.name }}
  {{- else if eq .type "command" }}
  exec:
    command:
      {{ .command | toYaml | nindent 6 }}
  {{- end }}
  failureThreshold: {{ default 1 .failureThreshold }}
  initialDelaySeconds: {{ default 5 .initialDelaySeconds }}
  periodSeconds: {{ default 5 .periodSeconds }}
  successThreshold: {{ default 1 .successThreshold }}
  timeoutSeconds: {{ default 5 .timeoutSeconds }}
  {{- end }}
  {{- end }}
  {{- end }}
  {{- end }}

{{- with .resources }}
resources:
  {{- toYaml . | nindent 2 }}
{{- end }}

{{- with .securityContext }}
securityContext:
  {{- toYaml . | nindent 2 }}
{{- end }}

{{- end -}}
