{{/*
Sinh YAML cho configMap từ file asset trong chart

Cách dùng: {{ template "common.configMaps" . }}
*/}}
{{- define "common.configMaps" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}

{{- $top := . }}
{{- range .Values.configMaps }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  {{- if .name }}
  name: {{ tpl .name $top }}
  {{- else }}
  name: {{ list $top .nameSuffix | include "common.elementName" }}
  {{- end }}
  labels:
    {{- include "common.labels" $top | nindent 4 }}
data:
  {{- if .envListPath }}
  {{- tpl ($top.Files.Get .envListPath) $top | nindent 2 }}
  {{- end }}
  {{- range $fileName, $filePath := .volumeList }}
  {{ $fileName }}: |-
    {{- tpl ($top.Files.Get $filePath) $top | nindent 4 }}
  {{- end }}
{{- end }}

{{- end -}}
{{- end -}}
