{{/*
Sinh YAML cho Pod Disruption Budget mặc định

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.pdb" . }}
*/}}
{{- define "common.pdb" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}
{{/* Lọc các value của riêng chart extend, lấy value mặc định của chart common và ghi đè các share value của chart extend so với chart common */}}

{{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
{{- if $deploy.podDisruptionBudget.enabled }}
---
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
  {{- block "common.pdb.spec" . }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  minAvailable: {{ $deploy.podDisruptionBudget.minAvailable }}
  selector:
    matchLabels:
      {{- include "common.selectorLabels" . | nindent 6 }}
  {{- end -}}
{{- end -}}

{{- end -}}
{{- end -}}
