{{/*
Sinh YAML cho service mặc địnhh

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.service" . }}
*/}}
{{- define "common.service" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}

---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
  {{- with .Values.service.annotations }}
  annotations:
    {{- tpl (toYaml .) $ | nindent 4 }}
  {{- end }}
spec:
  type: {{ .Values.service.type }}
  ports:
    {{- $app := default (dict) .Values.global.app | deepCopy | merge (default (dict) .Values.app | deepCopy) }}
    {{- range $app.ports }}
    {{- block "common.service.port" . }}
    - port: {{ default .internal .external }}
      name: {{ .name }}
      targetPort: {{ .internal }}
      protocol: {{ default "TCP" .protocol }}
    {{- end }}
    {{- end }}
  selector:
    {{- include "common.selectorLabels" . | nindent 4 }}
---
{{- if .Values.service.headless.enabled }}
apiVersion: v1
kind: Service
metadata:
  name: {{ list . "headless" | include "common.elementName" }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
  {{- with .Values.service.headless.annotations }}
  annotations:
    {{- tpl (toYaml .) $ | nindent 4 }}
  {{- end }}
spec:
  clusterIP: None
  type: {{ .Values.service.type }}
  ports:
    {{- $app := default (dict) .Values.global.app | deepCopy | merge (default (dict) .Values.app | deepCopy) }}
    {{- range $app.ports }}
    {{- include "common.service.port" . }}
    {{- end }}
  selector:
    {{- include "common.selectorLabels" . | nindent 4 }}
  {{- if eq .Values.service.type "LoadBalancer" }}
  loadBalancerIP: {{ .Values.service.ip }}
  {{- end }}
{{- end -}}

{{- end -}}
{{- end -}}
