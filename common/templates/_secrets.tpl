{{/*
Sinh YAML cho secret từ file asset trong chart

Cách dùng: {{ template "common.secrets" . }}
*/}}
{{- define "common.secrets" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}

{{- $top := . }}
{{- range .Values.secrets }}
---
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  annotations:
    sealedsecrets.bitnami.com/cluster-wide: "true"
  {{- if .name }}
  name: {{ tpl .name $top }}
  {{- else }}
  name: {{ list $top .nameSuffix | include "common.elementName" }}
  {{- end }}
  labels:
    {{- include "common.labels" $top | nindent 4 }}
spec:
  template:
    type: {{ default "Opaque" .type }}
    metadata:
      labels:
        {{- include "common.labels" $top | nindent 8 }}
  encryptedData:
    {{- if .envListPath }}
    {{- tpl ($top.Files.Get .envListPath) $top | nindent 4 }}
    {{- end }}
    {{- range $secretName, $filePath := .data }}
    {{ $secretName }}: |-
      {{- tpl ($top.Files.Get $filePath) $top | trim | nindent 6 }}
    {{- end }}
{{- end }}

{{- end -}}
{{- end -}}
