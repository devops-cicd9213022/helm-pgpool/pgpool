{{/*
Sinh YAML cho ingress mặc địnhh

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.ingress" . }}
*/}}
{{- define "common.ingress" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}

{{- $top := . }}
{{- $domain := include "common.envDomain" . }}
{{- if .Values.ingress.enabled }}
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
  annotations:
    {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
    {{- with .Values.ingress.annotations }}
    {{- tpl (toYaml .) $ | nindent 4 }}
    {{- end }}
spec:
  ingressClassName: {{ default "nginx" .Values.ingress.class }}
  tls:
    {{- if not .Values.ingress.disabledMainHost }}
    - hosts:
      - {{ $domain }}
      secretName: {{ default "" .Values.ingress.secret }}
    {{- end }}
    {{- range .Values.ingress.extraHosts }}
    {{- if ne .secret "" }}
    - hosts:
      - {{ printf `"%s"` .name }}
      secretName: {{ .secret }}
    {{- end }}
    {{- end }}
  rules:
    {{- if not .Values.ingress.disabledMainHost }}
    - host: {{ $domain }}
      http:
        paths:
          {{- range .Values.ingress.paths }}
          - path: {{ .endpoint }}
            pathType: {{ .type }}
            backend:
              service:
                name: {{ include "common.name" $top }}
                port:
                  name: {{ .portName }}
          {{- end }}
    {{- end }}
    {{- range .Values.ingress.extraHosts }}
    - host: {{ printf `"%s"` .name }}
      http:
        paths:
          {{- range .paths }}
          - path: {{ .endpoint }}
            pathType: {{ .type }}
            backend:
              service:
                name: {{ include "common.name" $top }}
                port:
                  name: {{ .portName }}
          {{- end }}
    {{- end }}
{{- end }}

{{- end -}}
{{- end -}}
