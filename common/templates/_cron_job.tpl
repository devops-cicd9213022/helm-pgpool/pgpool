{{/*
Sinh YAML cho cronJob mặc định

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.cronJob" . }}
*/}}
{{- define "common.cronJob" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}
{{/* Lọc các value của riêng chart extend, lấy value mặc định của chart common và ghi đè các share value của chart extend so với chart common */}}

---
{{- if semverCompare ">=1.21-0" .Capabilities.KubeVersion.Version }}
apiVersion: batch/v1
{{- else }}
apiVersion: batch/v1beta1
{{- end }}
kind: CronJob
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  {{- with $deploy.annotations }}
  annotations:
    {{- tpl (toYaml .) $ | nindent 4 }}
  {{- end }}
spec:
  failedJobsHistoryLimit: {{ default 10 $deploy.cron.failedJobsHistoryLimit }}
  successfulJobsHistoryLimit: {{ default 10 $deploy.cron.successfulJobsHistoryLimit }}
  {{- block "common.cronJob.spec" . }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  schedule: "{{ default "0 17 * * *" $deploy.cron.schedule }}"
  concurrencyPolicy: "{{ default "Forbid" $deploy.cron.concurrencyPolicy }}"
  jobTemplate:
    spec:
      parallelism: {{ default 1 $deploy.replicaCount }}
      template:
        {{- include "common.pod" . | nindent 8 }}
  {{- end -}}

{{- end -}}
{{- end -}}
