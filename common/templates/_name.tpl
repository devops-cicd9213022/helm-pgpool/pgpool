{{/*
Sinh tên resource từ chart release
*/}}
{{- define "common.name" -}}
{{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
{{- $name := default .Chart.Name (empty $deploy | ternary "" $deploy.suffix) }}
{{- eq .Release.Name $name | ternary $name (printf "%s-%s" .Release.Name $name) | include "common.dnsName" }}
{{- end }}

{{/*
Sinh tên resource và version từ chart release
*/}}
{{- define "common.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version }}
{{- end }}

{{/*
Tạo DNS name từ 1 string đầu vào.
Giúp tạo name hợp lệ trong Kubernetes.
Replace tất cả các ký tự không phù hợp thành _, và cắt ngắn chỉ còn 63 ký tự.

Cách dùng: {{ $input | include "common.dnsName" }}
*/}}
{{- define "common.dnsName" -}}
{{- . | replace " " "-" | replace "+" "-" | replace "." "-" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Tạo suffix ngẫu nhiên cho 1 string đầu vào.
Tương tự "common.dnsName" nhưng thêm hậu tố ngẫu nhiên 7 chữ cái/số

Cách dùng: {{ $input | include "common.suffixName" }}
*/}}
{{- define "common.suffixName" -}}
{{- . | replace " " "-" | replace "+" "-" | replace "." "-" | trunc 55 | trimSuffix "-" }}-{{ randAlphaNum 7 | lower }}
{{- end }}

{{/*
Sinh tên resource từ chart release và hậu tố chỉ định của resource

Cách dùng: {{ list $top $nameSuffix | include "common.elementName" }}
`$top` là biến chứa toàn bộ chart value
*/}}
{{- define "common.elementName" -}}
{{ printf "%s-%s" (include "common.name" (index . 0)) (index . 1) | include "common.dnsName" }}
{{- end }}

{{/*
Sinh tên resource từ chart release, tên chart con và hậu tố chỉ định của resource

Cách dùng: {{ list $top $parentNameSuffix | include "common.parentElementName" }}
`$top` là biến chứa toàn bộ chart value
*/}}
{{- define "common.parentElementName" -}}
{{ printf "%s-%s" (get (get (index . 0) "Release") "Name") (index . 1) | include "common.dnsName" }}
{{- end }}
