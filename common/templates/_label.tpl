{{/*
Label chung cho toàn bộ resource
*/}}
{{- define "common.labels" -}}
helm.sh/chart: {{ include "common.chart" . }}
{{ include "common.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/helm-revision: {{ .Release.Revision | quote }}
{{- end }}

{{/*
List các label phục vụ để query khi cần
*/}}
{{- define "common.selectorLabels" -}}
{{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
app.kubernetes.io/name: {{ eq .Release.Name .Chart.Name | ternary .Chart.Name (printf "%s-%s" .Release.Name .Chart.Name) | include "common.dnsName" }}
app.kubernetes.io/instance: {{ .Release.Name | include "common.dnsName" }}
{{- end }}
