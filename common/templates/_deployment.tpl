{{/*
Sinh YAML cho deployment mặc định

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.deployment" . }}
*/}}
{{- define "common.deployment" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}
{{/* Lọc các value của riêng chart extend, lấy value mặc định của chart common và ghi đè các share value của chart extend so với chart common */}}

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  {{- with $deploy.annotations }}
  annotations:
    {{- tpl (toYaml .) $ | nindent 4 }}
  {{- end }}
spec:
  {{- block "common.deployment.spec" . }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  # replicas: {{ default 1 $deploy.replicaCount }}
  {{- with $deploy.strategy }}
  strategy:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "common.selectorLabels" . | nindent 6 }}
  template:
    {{- include "common.pod" . | nindent 4 }}
  {{- end -}}

{{- end -}}
{{- end -}}
