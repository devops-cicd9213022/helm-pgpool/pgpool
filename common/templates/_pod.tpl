{{/*
Sinh YAML cho pod

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.pod" . }}
*/}}
{{- define "common.pod" -}}

metadata:
  labels:
    {{- include "common.selectorLabels" . | nindent 6 }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  {{- with $deploy.podAnnotations }}
  annotations:
    {{- tpl (toYaml .) $ | nindent 4 }}
  {{- end }}
spec:
  {{- block "common.pod.spec" . }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  restartPolicy: {{ $deploy.restartPolicy }}
  initContainers:
    {{- $initContainers := default (list) .Values.global.initContainers | concat (default (list) .Values.initContainers) }}
    {{- range $initContainers }}
    {{- $newTop := . | deepCopy }}
    - {{ dict "top" $ | deepCopy | merge $newTop | include "common.container" | indent 6 | trim }}
    {{- end }}
  containers:
    {{- $sidecarContainers := default (list) .Values.global.sidecarContainers | concat (default (list) .Values.sidecarContainers) }}
    {{- range $sidecarContainers }}
    {{- $newTop := . | deepCopy }}
    - {{ dict "top" $ | deepCopy | merge $newTop | include "common.container" | indent 6 | trim }}
    {{- end }}
    {{- $newTop := (default (dict) .Values.global.app | deepCopy | merge (default (dict) .Values.app | deepCopy)) | deepCopy }}
    - {{ dict "top" . | deepCopy | merge $newTop | include "common.container" | indent 6 | trim }}
  volumes:
    {{- include "common.volumes" . | nindent 4 }}
  {{- $security := (.Values.global.security | default (dict) | deepCopy | merge (.Values.security | default (dict) | deepCopy)) }}
  {{- if $security.serviceAccount.name }}
  serviceAccountName: {{ tpl $security.serviceAccount.name . }}
  {{- else if (ne .Release.Name .Chart.Name) }}
  serviceAccountName: {{ list . (default "" $security.serviceAccount.parentNameSuffix) | include "common.parentElementName" }}
  {{- else }}
  serviceAccountName: {{ list . (default "" $security.serviceAccount.nameSuffix) | include "common.elementName" }}
  {{- end }}
  imagePullSecrets:
    {{- range $security.imagePullSecrets }}
    {{- if .name }}
    - name: {{ tpl .name $ }}
    {{- else if (ne $.Release.Name $.Chart.Name) }}
    {{- if .nameSuffix }}
    - name: {{ list $ .nameSuffix | include "common.elementName" }}
    {{- end }}
    {{- if .parentNameSuffix }}
    - name: {{ list $ .parentNameSuffix | include "common.parentElementName" }}
    {{- end }}
    {{- else }}
    - name: {{ list $ .nameSuffix | include "common.elementName" }}
    {{- end }}
    {{- end }}
  {{- with $deploy.nodeSelector }}
  nodeSelector:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with $deploy.affinity }}
  affinity:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with $deploy.tolerations }}
  tolerations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with $deploy.hostAliases }}
  hostAliases:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with $security.securityContext }}
  securityContext:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- end }}

{{- end -}}
