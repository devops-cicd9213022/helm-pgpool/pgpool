{{/*
Sinh YAML cho volume của pod

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.volumes" . }}
*/}}
{{- define "common.volumes" -}}

{{- $top := . }}
{{- $storage := (.Values.global.storage | default (dict) | deepCopy | merge (.Values.storage | default (dict) | deepCopy)) }}
{{- range $storage.volumes }}
{{- if eq .type "configMap" }}
- name: {{ .mountName }}
  configMap:
    {{- if .name }}
    name: {{ tpl .name $top }}
    {{- else if (ne $top.Release.Name $top.Chart.Name) }}
    name: {{ list $top .parentNameSuffix | include "common.parentElementName" }}
    {{- else }}
    name: {{ list $top .nameSuffix | include "common.elementName" }}
    {{- end }}
    {{- $volumeSpec := omit . "type" "mountName" "nameSuffix" "parentNameSuffix" "name" }}
    {{- with $volumeSpec }}
        {{ toYaml $volumeSpec | nindent 4 }}
    {{- end }}
{{- else if eq .type "secret" }}
- name: {{ .mountName }}
  secret:
    {{- if .name }}
    secretName: {{ tpl .name $top }}
    {{- else if (ne $top.Release.Name $top.Chart.Name) }}
    secretName: {{ list $top .parentNameSuffix | include "common.parentElementName" }}
    {{- else }}
    secretName: {{ list $top .nameSuffix | include "common.elementName" }}
    {{- end }}
    {{- $volumeSpec := omit . "type" "mountName" "nameSuffix" "parentNameSuffix" "name" }}
    {{- with $volumeSpec }}
        {{ toYaml $volumeSpec | nindent 4 }}
    {{- end }}
{{- else if eq .type "persistentVolumeClaim" }}
- name: {{ .mountName }}
  persistentVolumeClaim:
    {{- if .name }}
    claimName: {{ tpl .name $top }}
    {{- else }}
    claimName: {{ list $top .nameSuffix | include "common.elementName" }}
    {{- end }}
{{- else }}
- name: {{ .mountName }}
  {{ .type }}:
    {{- $volumeSpec := omit . "type" "mountName" }}
    {{- with $volumeSpec }}
        {{ toYaml $volumeSpec | nindent 4 }}
    {{- end }}
{{- end }}
{{- end }}

{{- end -}}
