{{/*
Sinh YAML cho ServiceAccount mặc địnhh

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.serviceAccount" . }}
*/}}
{{- define "common.serviceAccount" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}

---
apiVersion: v1
kind: ServiceAccount
metadata:
  {{- block "common.serviceAccount.name" . }}
  {{- $security := (.Values.global.security | default (dict) | deepCopy | merge (.Values.security | default (dict) | deepCopy)) }}
  {{- if $security.serviceAccount.name }}
  name: {{ tpl $security.serviceAccount.name . }}
  {{- else if (ne .Release.Name .Chart.Name) }}
  name: {{ list . (default "" $security.serviceAccount.parentNameSuffix) | include "common.parentElementName" }}
  {{- else }}
  name: {{ list . (default "" $security.serviceAccount.nameSuffix) | include "common.elementName" }}
  {{- end }}
  {{- end }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
  {{- $security := (.Values.global.security | default (dict) | deepCopy | merge (.Values.security | default (dict) | deepCopy)) }}
  {{- with $security.serviceAccount.annotations }}
  annotations:
    {{- tpl (toYaml .) $ | nindent 4 }}
  {{- end }}

{{- end -}}
{{- end -}}
