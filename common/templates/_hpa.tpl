{{/*
Sinh YAML cho Horizontal Pod Autoscaler mặc định

Input là toàn bộ value của chart extend

Cách dùng: {{ template "common.hpa" . }}
*/}}
{{- define "common.hpa" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with mergeOverwrite $noValues $common $overrides -}}
{{/* Lọc các value của riêng chart extend, lấy value mặc định của chart common và ghi đè các share value của chart extend so với chart common */}}

{{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
{{- if $deploy.autoscaling.enabled }}
---
apiVersion: {{ default "autoscaling/v2beta2" $deploy.autoscaling.apiVersion }}
kind: HorizontalPodAutoscaler
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
  {{- block "common.hpa.spec" . }}
  {{- $deploy := default (dict) .Values.global.deploy | deepCopy | merge (default (dict) .Values.deploy | deepCopy) }}
  scaleTargetRef:
    apiVersion: apps/v1
    kind: {{ default "Deployment" $deploy.autoscaling.kind }}
    name: {{ include "common.name" . }}
  minReplicas: {{ default 1 $deploy.autoscaling.minReplicas }}
  maxReplicas: {{ $deploy.autoscaling.maxReplicas }}
  {{- with $deploy.autoscaling.metrics }}
  metrics:
    {{- toYaml . | nindent 4 }}
  {{- end -}}
  {{- end -}}
{{- end -}}

{{- end -}}
{{- end -}}
