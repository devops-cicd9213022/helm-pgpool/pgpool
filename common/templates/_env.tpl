{{/*
Trả về FQDN của project
*/}}
{{- define "common.envDomain" -}}
{{- if .Values.global.project.subDomain }}
  {{- tpl .Values.global.project.subDomain . }}.{{ tpl .Values.global.project.domain . -}}
{{- else }}
  {{- tpl .Values.global.project.domain . }}
{{- end }}
{{- end }}

{{/*
Trả về base domain dạng slug của project
*/}}
{{- define "common.slugDomain" -}}
  {{- tpl .Values.global.project.domain . | include "common.dnsName" }}
{{- end }}
