#!/bin/bash

# Kiểm tra xem đã truyền vào đúng số lượng tham số hay chưa
if [ $# -ne 2 ]; then
    echo "Sử dụng: $0 <đường_dẫn_env_yaml> <đường_dẫn_public_key>"
    exit 1
fi

# Đường dẫn đến file env.source.yaml và destination.env.yaml
yaml_file="$1"
public_key="$2"

# Kiểm tra xem các file tồn tại
if [ ! -f "$yaml_file" ]; then
    echo "File yaml $yaml_file không tồn tại."
    exit 1
fi

if [ ! -f "$public_key" ]; then
    echo "File public $public_key không tồn tại."
    exit 1
fi


# Đường dẫn đến file YAML
# yaml_file="/Users/hoanghoa/Documents/Code/Code_CMC/helm/charts/devops-cloud/cloudops-core/config-maps/dev/config-map.yaml"
# Kiểm tra xem file YAML tồn tại
if [ ! -f "$yaml_file" ]; then
  echo "File $yaml_file không tồn tại."
  exit 1
fi

while read line || [ -n "$line" ] ; do 
  # echo "$line" ; 
  key=$(echo "$line" | cut -d':' -f1 | tr -d '[:space:]')
  value=$(echo "$line" | cut -d':' -f2- | sed 's/^[[:space:]]*//')
  # encrypt=$(echo -n $value | kubeseal --raw --scope cluster-wide --from-file=/dev/stdin --cert=../../secret/cloudops-dev/public.pem)
  encrypt=$(echo -n $value | kubeseal --raw --scope cluster-wide --from-file=/dev/stdin --cert=$public_key)
  echo "$key: \"$encrypt\""
done < "$yaml_file"

