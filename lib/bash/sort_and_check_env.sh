#!/bin/bash

# purpose: consistency check env

# Kiểm tra xem đã truyền vào đúng số lượng tham số hay chưa
if [ $# -ne 2 ]; then
    echo "Sử dụng: $0 <đường_dẫn_env_source> <đường_dẫn_destination_env>"
    exit 1
fi

# Đường dẫn đến file env.source.yaml và destination.env.yaml
source_file="$1"
destination_file="$2"

# Kiểm tra xem các file tồn tại
if [ ! -f "$source_file" ]; then
    echo "File $source_file không tồn tại."
    exit 1
fi

if [ ! -f "$destination_file" ]; then
    echo "File $destination_file không tồn tại."
    exit 1
fi

# Lấy danh sách các khóa từ file env.source.yaml và sắp xếp chúng theo thứ tự
keys=$(grep "^[^#]" "$source_file" | cut -d: -f1 | awk '{print $1}')

# Tạo một biến tạm để lưu các giá trị đã sắp xếp
sorted_values=""

# Sắp xếp các giá trị dựa trên thứ tự của các khóa từ env.source.yaml
for key in $keys; do
    value=$(grep "^$key" "$destination_file" | awk -F ':' '{$1="";print}' | sed 's/^ *//')
    sorted_values+="$key: $value\n"
done

# Lấy các giá trị không có trong env.source.yaml và thêm vào cuối danh sách
non_source_keys=$(grep -v -f <(grep "^[^#]" "$source_file" | cut -d: -f1 | awk '{print "^"$1":"}') "$destination_file" | grep "^[^#]")

if [ ! -z "$non_source_keys" ]; then
    sorted_values+="# Values exist destination, not exist source !!! \n"
    sorted_values+="$non_source_keys"
fi

# Ghi kết quả vào file mới
sorted_file="sorted_destination.env.yaml"
echo -e "$sorted_values" > "$sorted_file"

# Hoàn thành thông báo
echo "Đã sắp xếp lại $destination_file thành $sorted_file."
