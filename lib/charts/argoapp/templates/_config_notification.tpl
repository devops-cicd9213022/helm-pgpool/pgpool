{{- define "argoapp.confignotification" }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-notifications-cm
  namespace: argocd
data:

  context: |
    environmentName: {{ .Chart.Name }}

  service.webhook.discord: |
    url: https://api.telegram.org/bot6466300817:AAEC5UEodhy8gn0C_DUD-0dZiALF2-IoxCg/sendMessage
    headers:
    - name: Content-Type
      value: application/json   
  trigger.on-sync-succeeded: |
    - description: Application syncing has succeeded
      send:
      - app-noti
      when: app.status.operationState.phase in ['Succeeded']
  trigger.on-deployed: |
    - description: Application is synced and healthy. Triggered once per commit.
      oncePer: app.status.sync.revision
      send:
      - app-noti
      when: app.status.operationState.phase in ['Succeeded'] and app.status.health.status == 'Healthy'
  trigger.on-health-degraded: |
    - description: Application has degraded
      send:
      - app-noti
      when: app.status.health.status == 'Degraded'
  trigger.on-sync-failed: |
    - description: Application syncing has failed
      send:
      - app-noti
      when: app.status.operationState.phase in ['Error', 'Failed']
  trigger.on-sync-running: |
    - description: Application is being synced
      send:
      - app-noti
      when: app.status.operationState.phase in ['Running']
  trigger.on-sync-status-unknown: |
    - description: Application status is 'Unknown'
      send:
      - app-noti
      when: app.status.sync.status == 'Unknown'
  template.app-noti: |
    webhook:
      discord:
        method: POST
        body: |
            {
              "text": "{{- $rawText := "CD: {{ .context.environmentName }} - App: {{ .app.metadata.name }}. Message: {{(call .repo.GetCommitMetadata .app.status.sync.revision).Message}}. phase at {{.app.status.operationState.phase}} {{.app.status.sync.revision}}" -}}{{ $rawText }}",
              "chat_id": "-4011936012"
            }


{{- end }}
