{{- define "argoapp.namespace" }}
---
apiVersion: v1
kind: Namespace
metadata:
  name: {{ .namespace }}
{{- end }}