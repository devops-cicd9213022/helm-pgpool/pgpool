
{{- define "argoapp.applications" }}
{{- $top := . }}
{{- range .releases }}
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: {{ $top.namespace}}-{{ .release }}
  namespace: argocd
  finalizers:
    - resources-finalizer.argocd.argoproj.io
  annotations:
    # notifications.argoproj.io/subscribe.on-sync-succeeded.discord: ""
    # notifications.argoproj.io/subscribe.on-deployed.discord: ""
    notifications.argoproj.io/subscribe.on-health-degraded.discord: ""
    notifications.argoproj.io/subscribe.on-sync-failed.discord: ""
    notifications.argoproj.io/subscribe.on-sync-running.discord: ""
    # notifications.argoproj.io/subscribe.on-sync-status-unknown.discord: ""
  
spec:
  project: {{ $top.namespace }}
  {{ if eq .syncPolicy true }}
  syncPolicy:
    {{- if .syncOptions }}
    syncOptions:
      {{ toYaml .syncOptions | nindent 4 | trim }}
    {{- end }}
    {{- $automated := dict "prune" true "selfHeal" true }}
    automated:
      {{ default $automated .automated | toYaml | nindent 6 | trim }}
  {{ else if eq .syncPolicy false }}
  syncPolicy: {}
    # automated: null
    # manual: {}    
  {{ else }}
  syncPolicy:
    {{- if .syncOptions }}
    syncOptions:
      {{ toYaml .syncOptions | nindent 4 | trim }}
    {{- end }}
    {{- $automated := dict "prune" true "selfHeal" true }}
    automated:
      {{ default $automated .automated | toYaml | nindent 6 | trim }}
  {{ end }}
  source:
    repoURL: https://gitlab.savis.vn/devsecops/cd/devsecops-cd-helm-dc.git
    targetRevision: main
    path: charts/{{ .chartPath }}
    helm:
      releaseName: {{ .release }}
      valueFiles:
        - values.yaml
        {{- if .valueFileSuffix }}
        - values-{{ $top.valueFileSuffix }}-{{ .valueFileSuffix }}.yaml
        {{- else }}
        - values-{{ $top.valueFileSuffix }}.yaml
        {{- end }}
      skipCrds: {{ not (default false $top.clusterWide) }}
  destination:
    server: https://kubernetes.default.svc
    namespace: {{ $top.namespace }}
{{- end }}
{{- end }}
