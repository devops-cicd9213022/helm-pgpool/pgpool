{{- define "argoapp.appproject" }}
---
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: {{ .namespace }}
  namespace: argocd
spec:
  description: {{ .description }}
  destinations:
    - namespace: {{ .namespace }}
      server: https://kubernetes.default.svc
  {{- if .clusterWide }}
  clusterResourceWhitelist:
    - group: '*'
      kind: '*'
  {{- end }}
  namespaceResourceWhitelist:
    - group: '*'
      kind: '*'
  orphanedResources:
    warn: true
  sourceNamespaces:
    - {{ .namespace }}
  sourceRepos:
    - '*'
{{- end }}
