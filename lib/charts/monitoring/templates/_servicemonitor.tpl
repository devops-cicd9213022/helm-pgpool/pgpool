{{- define "argoapp.servicemonitor" -}}

---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: {{ .name }}
  namespace: monitoring
  labels:
    app: {{ .name }}
spec:
  endpoints:
    - port: metrics
      interval: {{ .interval }}
      path: {{ .path }}
  selector:
    matchLabels:
      app: {{ .name }}
  namespaceSelector:
    matchNames:
      - monitoring
{{- end }}