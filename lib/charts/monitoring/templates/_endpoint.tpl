{{- define "argoapp.endpoint" -}}

---
apiVersion: v1
kind: Endpoints
metadata:
  name: {{ .name }}
  namespace: monitoring
subsets:
  - addresses:
    - ip: {{ .addresses }}
    ports:
      - name: metrics
        port: {{ .ports }}
        protocol: TCP
{{- end }}