{{- define "argoapp.service" -}}

---
apiVersion: v1
kind: Service
metadata:
  name: {{ .name }}
  namespace: monitoring
  labels:
    app: {{ .name }}
spec:
  ports:
    - name: metrics
      port: {{ .ports }}
      protocol: TCP
      targetPort: {{ .ports }}
{{- end }}
